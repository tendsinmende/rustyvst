#[macro_use]
extern crate vst;
extern crate rand;
extern crate miparse;
extern crate miprocessor;

use miprocessor::*;

use vst::plugin::{Info, Plugin, Category, PluginParameters};
use vst::buffer::AudioBuffer;
use vst::event::Event;
use vst::api::Events;
use rand::random;

use std::f64::consts::PI;
use std::sync::Arc;
use vst::util::AtomicFloat;

use std::convert::TryInto;

pub const TAU: f64 = PI * 2.0;

pub struct EnvelopeParameter{
    attack: AtomicFloat,
    decay: AtomicFloat,
    sustain: AtomicFloat,
    release: AtomicFloat
}

impl EnvelopeParameter{
    ///Returns how much of the signal has to be returned based on this envelope filter.
    fn get_alpha(&self, note_duration: f64, state: &NoteState) -> f64{

        //First calculate the level of this note at this time, if it was played.
        // If there was the release event in between, scale down
        
        let note_alpha_on = {
            let duration = match state{
                NoteState::On(_start) => note_duration,
                NoteState::Off{on_stamp, off_stamp} => off_stamp - on_stamp
            };

                
            let attack = self.attack.get() as f64;
            let decay = self.decay.get() as f64;
            let sustain = self.sustain.get() as f64;
            
            if duration < attack{
                duration / attack
            }else if duration < (attack + decay){
                let decay_perc = (duration - attack) / decay;
                
                1.0 - decay_perc * (1.0 - sustain)
            }else{
                sustain
            }
        };
        
        match state{
            NoteState::On(_start) => {
                note_alpha_on
            },
            NoteState::Off{on_stamp, off_stamp} => {

                //Scale the alpha down based on the release
                                
                let release = self.release.get();
                let release_duration = note_duration - (off_stamp - on_stamp);
                note_alpha_on * ((1.0 - (release_duration / release as f64))).max(0.0)
            }
        }
        
    }
}

impl Default for EnvelopeParameter{
    fn default() -> EnvelopeParameter{
        EnvelopeParameter{
            attack: AtomicFloat::new(1.0),
            decay: AtomicFloat::new(0.5),
            sustain: AtomicFloat::new(0.75),
            release: AtomicFloat::new(0.5),
        }
    }
}


impl PluginParameters for EnvelopeParameter {
    // the `get_parameter` function reads the value of a parameter.
    fn get_parameter(&self, index: i32) -> f32 {
        match index {
            0 => self.attack.get(),
            1 => self.decay.get(),
            2 => self.sustain.get(),
            3 => self.release.get(),
            _ => 0.0,
        }
    }

    // the `set_parameter` function sets the value of a parameter.
    fn set_parameter(&self, index: i32, val: f32) {
        #[allow(clippy::single_match)]
        match index {
            0 => self.attack.set(val),
            1 => self.decay.set(val),
            2 => self.sustain.set(val),
            3 => {self.release.set(val)},
            _ => (),
        }
    }

    // This is what will display underneath our control.  We can
    // format it into a string that makes the most since.
    fn get_parameter_text(&self, index: i32) -> String {
        match index {
            0 => format!("{:.2}", self.attack.get()),
            1 => format!("{:.2}", self.decay.get()),
            2 => format!("{:.2}", self.sustain.get()),
            3 => format!("{:.2}", self.release.get()),

            _ => "".to_string(),
        }
    }

    // This shows the control's name.
    fn get_parameter_name(&self, index: i32) -> String {
        match index {
            0 => "Attack",
            1 => "Decay",
            2 => "Sustain",
            3 => "Release",
            _ => "",
        }
        .to_string()
    }
}

#[derive(Default)]
struct Whisper {
    sample_rate: f64,
    time: f64,
    
    note_processor: MidiProcessor,

    //The envelope parameter
    env_param: Arc<EnvelopeParameter>
}

impl Whisper{
    fn time_per_sample(&self) -> f64{
        1.0 / self.sample_rate
    }
    
}

// We're implementing a trait `Plugin` that does all the VST-y stuff for us.
impl Plugin for Whisper {
    
    ///Returns main info of this plugin
    fn get_info(&self) -> Info {
        Info {
            name: "SinWave".to_string(),

            // Used by hosts to differentiate between plugins.
            unique_id: 1337, 

            // We don't need inputs
            inputs: 0,

            //Use one midi input
            midi_inputs: 1,
            
            // We do need two outputs though.  This is default, but let's be 
            // explicit anyways.
            outputs: 2,

            //Shows four parameters
            parameters: 4,

            // Set our category
            category: Category::Synth,
            
            // We don't care about other stuff, and it can stay default.
            ..Default::default()
        }
    }

    fn get_parameter_object(&mut self) -> Arc<dyn PluginParameters> {
        Arc::clone(&self.env_param) as Arc<dyn PluginParameters>
    }

    // Here's the function that allows us to receive events
    fn process_events(&mut self, events: &Events) {

        // Some events aren't MIDI events - so let's do a match
        // to make sure we only get MIDI, since that's all we care about.

        let mut midi_events = Vec::new();
        
        for event in events.events() {
            match event {
                Event::Midi(ev) => {

                    let midi_message = if let Some(m) = miparse::MidiMessage::from_buffer(ev.data){
                        m
                    }else{
                        continue;
                    };

                    midi_events.push(midi_message);
                },
                // We don't care if we get any other type of event
                _ => (),
            }
        }

        self.note_processor.process_events(self.time, &midi_events);
    }

    fn set_sample_rate(&mut self, rate: f32) {
        self.sample_rate = f64::from(rate);
    }
    
    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {

        //Set the note_processors timeout for released notes to the release time
        self.note_processor.set_max_duration_after_release(self.env_param.release.get() as f64);
        
        //Get the buffers
        let samples = buffer.samples();
        let (_, mut output_buffer) = buffer.split();
        //Time per sample to play
        let per_sample = self.time_per_sample();
        let buffer_len = output_buffer.len();
        
        for sample_idx in 0..samples{
            //The time for this sample
            let time = self.time;

            let mut sample_signal = self.note_processor.process_notes(time, |sample_time, note_state, note|{
                //Calculate frequency sample via sin(time * freq * 2pi);
                let signal = (time * miparse::midi_pitch_to_freq(note.key) * TAU).sin();

                //Small ghetto envelope, later maybe in some settings?
                let alpha = self.env_param.get_alpha(note_state.duration(sample_time), note_state);
    
                (signal * alpha)
            });

            if sample_signal > 1.0 {
                sample_signal = 0.0;
            }
            
            //Advance inner time and note duration before returning sample
            self.time += per_sample;

            //Push this sample into all output buffer.
            for buf_idx in 0..buffer_len{
                output_buffer.get_mut(buf_idx)[sample_idx] = sample_signal as f32;
            }
        }
    }
}

plugin_main!(Whisper); 
