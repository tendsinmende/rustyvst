extern crate miparse;
use miparse::*;

use std::collections::HashMap;

#[derive(Clone,Copy,Debug)]
pub enum NoteState{
    On(f64),
    Off{
        on_stamp: f64,
        off_stamp: f64
    }
}

impl NoteState{
    pub fn duration(&self, current_time: f64) -> f64{
        match self{
            NoteState::On(start) => current_time - start,
            NoteState::Off{on_stamp, off_stamp: _} => current_time - on_stamp
        }
    }

    pub fn duration_since_end(&self, current_time: f64) -> f64{
        match self{
            NoteState::On(_start) => 0.0,
            NoteState::Off{on_stamp, off_stamp} => current_time - (off_stamp - on_stamp) 
        }
    }
}

pub struct MidiProcessor{
    ///Collects all active notes. There can only be one active note per key
    active_notes: HashMap<u8, (Note, NoteState)>,
    ///Collects all released notes that might still have some for of audio signal going.
    ///Several notes of the same key can land here, for instance if a really long reverb is used.
    released_notes: Vec<(Note, NoteState)>,

    ///How long a released note can create a new signal tone, before it can be savely deleted.
    note_max_after_release_duration: f64,
}

impl Default for MidiProcessor{
    fn default() -> MidiProcessor{
        MidiProcessor{
            active_notes: HashMap::new(),
            released_notes: Vec::new(),
            note_max_after_release_duration: 1.0
        }
    }
}

impl MidiProcessor{
    pub fn new() -> Self{
        MidiProcessor{
            active_notes: HashMap::new(),
            released_notes: Vec::new(),
            note_max_after_release_duration: 1.0
        }
    }

    pub fn process_events(&mut self, time: f64, messages: &[MidiMessage]){       
        for message in messages{
            match message{
                MidiMessage::NoteOn(note) => {
                    let _ = self.active_notes.insert(note.key, (*note, NoteState::On(time)));
                },
                MidiMessage::NoteOff(note) => {
                    let (note, state) = if let Some(note_state) = self.active_notes.remove(&note.key){
                        note_state
                    }else{
                        println!("Warning could not find note {} since its not active, but was released!", note.key);
                        continue;
                    };

                    let old_start = match state{
                        NoteState::On(i) => i,
                        NoteState::Off{on_stamp, off_stamp: _} => on_stamp,
                    };
                    
                    let new_state = NoteState::Off{
                        on_stamp: old_start,
                        off_stamp: time
                    };

                    self.released_notes.push((note, new_state));
                },
                _ => println!("Ignoring midi signal"),
            }
        }
    }

    pub fn set_max_duration_after_release(&mut self, max_duration: f64){
        self.note_max_after_release_duration = max_duration;
    }

    pub fn process_notes<F>(&self, time: f64, mut func: F) -> f64
    where F: FnMut(f64, &NoteState, &Note) -> f64{
        //The whole signal
        let mut main_sig: f64 = 0.0;
        //Number of notes that contributed
        let mut num_notes = 0;
        for (_key, (note, state)) in self.active_notes.iter(){
            let sig = func(time, state, note);
            if sig.abs() != 0.0{                
                main_sig += sig;
                num_notes += 1;   
            }
        }

        for (note, state) in self.released_notes.iter(){
            let sig = func(time, state, note);
            if sig.abs() != 0.0{
                main_sig += sig;
                num_notes += 1;   
            }
        }

        if num_notes > 0{
            main_sig / num_notes as f64
        }else{
            0.0
        }
    }
}

