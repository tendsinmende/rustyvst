
const NOTE_OFF: u8 = 0b1000_0000;
const NOTE_ON: u8 = 0b1001_0000;
const POLYPHONIC_KEY_PRESSURE: u8 = 0b1010_0000;
const CONTROL_CHANGE_OR_CHANNEL_MODE_MESSAGE: u8 = 0b1011_0000;
const PROGRAM_CHANGE: u8 = 0b1100_0000;
const CHANNEL_PRESSURE: u8 = 0b1101_0000;
const PITCH_BEND_CHANGE: u8 = 0b1110_0000;
const SYSTEM_EXCLUSIVE: u8 = 0b1111_0000;
const TIME_CODE_QUARTER_FRAME: u8 = 0b1111_0001;
const SONG_POSITION_POINTER: u8 = 0b1111_0010;
const SONG_SELECT: u8 = 0b1111_0011;
const UNDEFINED_2: u8 = 0b1111_0100;
const UNDEFINED_1: u8 = 0b1111_0100;
const TUNE_REQUEST: u8 = 0b1111_0110;
const END_OF_EXCLUSIVE: u8 = 0b1111_0111;

const TIMING_CLOCK: u8 = 0b1111_1000;
const UNDEFINED_3: u8 = 0b1111_1001;
const START: u8 = 0b1111_1010;
const CONTINUE: u8 = 0b1111_1011;
const STOP: u8 = 0b1111_1100;
const UNDEFINED_4: u8 = 0b1111_1101;

const ACTIVE_SENSING: u8 = 0b1111_1110;
const RESET: u8 = 0b1111_1111;

use std::f64::consts::PI;

/// Convert the midi note's pitch into the equivalent frequency.
///
/// This function assumes A4 is 440hz.
pub fn midi_pitch_to_freq(pitch: u8) -> f64 {
    const A4_PITCH: i8 = 69;
    const A4_FREQ: f64 = 440.0;

    // Midi notes can be 0-127
    ((f64::from(pitch as i8 - A4_PITCH)) / 12.).exp2() * A4_FREQ
}

///Represents a single note. On how to convert the key value into an actual key, see the key table here: http://www.petesqbsite.com/sections/express/issue18/midifilespart1.html
#[derive(Default, Clone, Copy, Debug)]
pub struct Note{
    pub key: u8,
    pub velocity: u8
}

///An opaque Midi message.
///
/// *WARNING:* Not all messages are currently implemented!
pub enum MidiMessage{
    NoteOff(Note),
    NoteOn(Note),
    PolyNoteKeyPressure(Note),
    Start,
    Continue,
    Stop,
    Reset
}

impl MidiMessage{
    /// Parses a given byte triple into a Midi Message.
    /// Returns None if the message is not implemented or undefined
    pub fn from_buffer(buf: [u8;3]) -> Option<Self>{
        let message = match buf[0]{
            NOTE_OFF => MidiMessage::NoteOff(Note{key: buf[1], velocity: buf[2]}),
            NOTE_ON => MidiMessage::NoteOn(Note{key: buf[1], velocity: buf[2]}),
            POLYPHONIC_KEY_PRESSURE => MidiMessage::PolyNoteKeyPressure(Note{key: buf[1], velocity: buf[2]}),
            START => MidiMessage::Start,
            STOP => MidiMessage::Stop,
            CONTINUE => MidiMessage::Continue,
            RESET => MidiMessage::Reset,
            _ => return None,
        };

        Some(message)
    }
}
